/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JList;

import interfaces.PizzaComponent;
import interfaces.PizzaDecorator;

/**
 *
 * @author sandroandrade
 */
public class MainWindow extends javax.swing.JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Creates new form MainWindow
	 */
	public MainWindow() {
		initComponents();
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	
	// <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
	private void initComponents() {

		jPanel3 = new javax.swing.JPanel();
		jScrollPane1 = new javax.swing.JScrollPane();
		availableDecorators = new javax.swing.JList<>();
		jScrollPane2 = new javax.swing.JScrollPane();
		selectedDecorators = new javax.swing.JList<>();
		jPanel1 = new javax.swing.JPanel();
		insertButton = new javax.swing.JButton();
		removeButton = new javax.swing.JButton();
		jPanel2 = new javax.swing.JPanel();
		upButton = new javax.swing.JButton();
		downButton = new javax.swing.JButton();
		jButton1 = new javax.swing.JButton();

		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		getContentPane().setLayout(new java.awt.GridLayout());

		availableDecorators.setModel(new DefaultListModel<String>());
		selectedDecorators.setModel(new DefaultListModel<String>());
		//	new javax.swing.AbstractListModel<String>() {
		// /**
		//	 * 
		//	 */
		//	private static final long serialVersionUID = 1L;
		//	String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
		//    public int getSize() { return strings.length; }
		//    public String getElementAt(int i) { return strings[i]; }
		//} );
		DefaultListModel<String> listModel = (DefaultListModel<String>) availableDecorators.getModel();
		decoratorList = DecoratorLoader.load();
		//System.out.println(decoratorList);
		for(Class<PizzaDecorator> decorator : decoratorList) {
			String sabor = decorator.toString().split("\\.")[1];
			listModel.addElement(sabor.substring(0, sabor.length()-9));
		}
		//listModel.addElement("Hey");
		//listModel.removeElement("Hello");
		jScrollPane1.setViewportView(availableDecorators);

		jScrollPane2.setViewportView(selectedDecorators);

		jPanel1.setLayout(new java.awt.GridLayout(2, 1));

		insertButton.setText(">");
		insertButton.addActionListener(new ActionListener(){

			JList<String> availableDecorators;
			JList<String> selectedDecorators;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				for(String item : availableDecorators.getSelectedValuesList()) {
					((DefaultListModel<String>) availableDecorators.getModel()).removeElement(item);
					((DefaultListModel<String>) selectedDecorators.getModel()).addElement(item);
				}
			}

			public ActionListener init(JList<String> availableDecorators, JList<String> selectedDecorators) {
				this.availableDecorators = availableDecorators;
				this.selectedDecorators = selectedDecorators;
				return this;
			}

		}.init(availableDecorators, selectedDecorators));


		jPanel1.add(insertButton);

		removeButton.setText("<");
		removeButton.addActionListener(new ActionListener(){

			JList<String> availableDecorators;
			JList<String> selectedDecorators;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				for(String item : selectedDecorators.getSelectedValuesList()) {
					((DefaultListModel<String>) availableDecorators.getModel()).addElement(item);
					((DefaultListModel<String>) selectedDecorators.getModel()).removeElement(item);
				}
			}

			public ActionListener init(JList<String> availableDecorators, JList<String> selectedDecorators) {
				this.availableDecorators = availableDecorators;
				this.selectedDecorators = selectedDecorators;
				return this;
			}

		}.init(availableDecorators, selectedDecorators));

		jPanel1.add(removeButton);

		jPanel2.setLayout(new java.awt.GridLayout(2, 1));

		upButton.setText("Up");
		
		upButton.addActionListener(new ActionListener(){

			JList<String> selectedDecorators;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				List<String> list = selectedDecorators.getSelectedValuesList();
				for(String item : list) {
					int index = ((DefaultListModel<String>) selectedDecorators.getModel()).indexOf(item);
					if(index != 0) {
						((DefaultListModel<String>) selectedDecorators.getModel()).removeElement(item);
						((DefaultListModel<String>) selectedDecorators.getModel()).add(index-1, item);
					}
				}
			}

			public ActionListener init(JList<String> selectedDecorators) {
				this.selectedDecorators = selectedDecorators;
				return this;
			}

		}.init(selectedDecorators));
		
		jPanel2.add(upButton);

		downButton.setText("Down");
		
		
		downButton.addActionListener(new ActionListener(){

			JList<String> selectedDecorators;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				List<String> list = selectedDecorators.getSelectedValuesList();
				ArrayList<String> reverse = new ArrayList<String>();
				for(String item : list)
					reverse.add(0, item);
				for(String item : reverse) {
					int index = ((DefaultListModel<String>) selectedDecorators.getModel()).indexOf(item);
					if(index != selectedDecorators.getModel().getSize() - 1) {
						((DefaultListModel<String>) selectedDecorators.getModel()).removeElement(item);
						((DefaultListModel<String>) selectedDecorators.getModel()).add(index + 1, item);
					}
				}
			}

			public ActionListener init(JList<String> selectedDecorators) {
				this.selectedDecorators = selectedDecorators;
				return this;
			}

		}.init(selectedDecorators));
		
		jPanel2.add(downButton);

		jButton1.setText("Ok");
		jButton1.addActionListener((new ActionListener(){

			JList<String> selectedDecorators;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				PizzaComponent pizza = new PizzaSimples();
				for(Object item : ((DefaultListModel<String>) selectedDecorators.getModel()).toArray()) {
					for(Class<PizzaDecorator> decorator : decoratorList)
						if(decorator.toString().split("\\.")[1].equals((String)item + "Decorator"))
							try {
								Class<?>[] decoratorClass = new Class[1];
								decoratorClass[0] = PizzaComponent.class;
								pizza = decorator.getDeclaredConstructor(decoratorClass).newInstance(pizza);
								//pizza = decorator.newInstance((PizzaDecorator) pizza);
							} catch (NoSuchMethodException | SecurityException | InstantiationException
									| IllegalAccessException | IllegalArgumentException
									| InvocationTargetException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
				}
				pizza.preparar();
			}

			public ActionListener init(JList<String> selectedDecorators) {
				this.selectedDecorators = selectedDecorators;
				return this;
			}

		}.init(selectedDecorators)));

		javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
		jPanel3.setLayout(jPanel3Layout);
		jPanel3Layout.setHorizontalGroup(
				jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jPanel3Layout.createSequentialGroup()
						.addContainerGap()
						.addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
										.addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 112, Short.MAX_VALUE)
										.addGap(18, 18, 18)
										.addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
										.addGap(25, 25, 25)
										.addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
										.addGap(18, 18, 18)
										.addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
								.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
										.addGap(0, 0, Short.MAX_VALUE)
										.addComponent(jButton1)))
						.addContainerGap())
				);
		jPanel3Layout.setVerticalGroup(
				jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jPanel3Layout.createSequentialGroup()
						.addContainerGap()
						.addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
										.addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
										.addComponent(jPanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
										.addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 112, Short.MAX_VALUE)
						.addComponent(jButton1)
						.addContainerGap())
				);

		getContentPane().add(jPanel3);
		setTitle("Pizzaria");

		pack();
	}// </editor-fold>//GEN-END:initComponents

	public void addDecorator(List<String> values) {

	}

	public void removeDecorator(List<String> values) {

	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		/* Set the Nimbus look and feel */
		//<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
		/* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
		 * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
		 */
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}
		//</editor-fold>

		/* Create and display the form */
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new MainWindow().setVisible(true);
			}
		});
	}

	// Variables declaration - do not modify//GEN-BEGIN:variables
	private javax.swing.JList<String> availableDecorators;
	private javax.swing.JButton downButton;
	private javax.swing.JButton insertButton;
	private javax.swing.JButton jButton1;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JPanel jPanel2;
	private javax.swing.JPanel jPanel3;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JScrollPane jScrollPane2;
	private javax.swing.JButton removeButton;
	private javax.swing.JList<String> selectedDecorators;
	private javax.swing.JButton upButton;

	ArrayList<Class<PizzaDecorator>> decoratorList;

	// End of variables declaration//GEN-END:variables


}
