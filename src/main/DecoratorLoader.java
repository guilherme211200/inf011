package main;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;

import interfaces.PizzaDecorator;


public class DecoratorLoader {

	public static ArrayList<Class<PizzaDecorator>> load() {

		File currentDir = new File("./plugins");
		String []plugins = currentDir.list();
		URL[] jars = new URL[plugins.length];
		for (int i = 0; i < plugins.length; i++)
		{
			System.out.println(i+1 + " - " + plugins[i].split("\\.")[0]);
			try {
				jars[i] = (new File("./plugins/" + plugins[i])).toURL();
			} catch (MalformedURLException e1) {
				e1.printStackTrace();
			}
		}
		URLClassLoader ulc = new URLClassLoader(jars);
		
		ArrayList<Class<PizzaDecorator>> decoratorList = new ArrayList<Class<PizzaDecorator>>();

		for(String pluginName : plugins)
			try {

				@SuppressWarnings("unchecked")
				Class<PizzaDecorator> classDecorator =  (Class<PizzaDecorator>) Class.forName
						//("com.Unidade",
						("decorators." + pluginName.split("\\.")[0],
								true, ulc);
				//Method method = metaFactory.getDeclaredMethod("newInstance");
				//method.invoke(metaFactory);
				decoratorList.add(classDecorator);
			} catch (ClassNotFoundException | SecurityException | IllegalArgumentException e1) {
				e1.printStackTrace();
			}

		return decoratorList;

	}
	
	public static void main(String[] args) {
		for(Object decorator : load())
			System.out.println(decorator.toString().split("\\.")[1]);
	}

}
